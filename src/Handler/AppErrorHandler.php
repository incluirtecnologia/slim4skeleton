<?php

declare(strict_types=1);

namespace Intec\Slim4Skeleton\Handler;

use Slim\Exception\HttpNotFoundException;
use Slim\Handlers\ErrorHandler;

class AppErrorHandler extends ErrorHandler
{
    private $ignoreExceptions = [
        HttpNotFoundException::class
    ];

    protected function logError(string $error): void
    {
        $exceptionClass = get_class($this->exception);

        if (in_array($exceptionClass, $this->ignoreExceptions)) {
            return;
        }

        parent::logError($error);
    }
}
