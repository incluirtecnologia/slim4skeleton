<?php

declare(strict_types=1);


namespace Intec\Slim4Skeleton\Action;

use Intec\Slim4Skeleton\Renderer\JsonResponseRenderer;
use Psr\Http\Message\ResponseInterface;

abstract class Action
{
    protected function toJson(ResponseInterface $response, array $data = [], string $message = 'ok', int $httpCode = 200): ResponseInterface
    {
        return JsonResponseRenderer::toJson($response, $data, $message, $httpCode);
    }
}
