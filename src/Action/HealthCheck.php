<?php

declare(strict_types=1);


namespace Intec\Slim4Skeleton\Action;

use Psr\Http\Message\ResponseInterface;

class HealthCheck extends Action
{
    public function __invoke(ResponseInterface $response)
    {
        return $this->toJson($response);
    }
}