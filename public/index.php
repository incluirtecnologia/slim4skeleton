<?php

require __DIR__ . '/../vendor/autoload.php';

// Set up settings & dependencies
$settings = require __DIR__ . '/../config/settings.php';
$dependencies = require __DIR__ . '/../config/dependencies.php';
$routes = require __DIR__ . '/../config/routes.php';
$appBuilder = require __DIR__ . '/../config/app.php';

$app = $appBuilder($settings, $dependencies, $routes);

$app->run();
