

### Migrações

```
vendor/bin/doctrine-migrations migrations:migrate
```

Referência: https://www.doctrine-project.org/projects/doctrine-migrations/en/3.0/reference/introduction.html#introduction


### ORM

Verificar se o banco de dados está atualizado

```
vendor/bin/doctrine orm:validate-schema
```