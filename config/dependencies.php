<?php

declare(strict_types=1);

use Doctrine\ORM\EntityManager;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

use function DI\factory;

return [
    LoggerInterface::class => function (ContainerInterface $c) {
        $loggerPath = $c->get('logger.path');
        $loggerLevel = $c->get('logger.level');
        $logger = new Logger($c->get('logger.name'));

        $processor = new UidProcessor();
        $logger->pushProcessor($processor);

        $handler = new StreamHandler($loggerPath, $loggerLevel);
        $logger->pushHandler($handler);

        return $logger;
    },
    EntityManager::class => factory(function (ContainerInterface $c) {
        $config = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
            $c->get('doctrine.meta.entity_path'),
            $c->get('doctrine.meta.auto_generate_proxies'),
            $c->get('doctrine.meta.proxy_dir'),
            $c->get('doctrine.meta.cache'),
            false
        );

        return EntityManager::create($c->get('doctrine.connection'), $config);
    }),
];
