<?php

declare(strict_types=1);

use Intec\Slim4Skeleton\Action\HealthCheck;
use Slim\App;

/**
 * Para mais informações de como criar rotas consulte a documentação do Slim:
 *
 * http://www.slimframework.com/docs/v4/objects/routing.html
 *
 */
return function (App $app) {
    $app->get('/healthz', HealthCheck::class);
};