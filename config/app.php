<?php

declare(strict_types=1);

use Intec\Slim4Skeleton\Handler\AppErrorHandler;
use Intec\Slim4Skeleton\Renderer\JsonErrorRenderer;
use Psr\Log\LoggerInterface;

return function (array $settings, array $dependencies, Closure $routes) {

    $builder = new \DI\ContainerBuilder();
    $builder->useAutowiring(true);
    $builder->useAnnotations(false);

    if ($settings['app.cache_enabled']) {
        $builder->enableCompilation(__DIR__ . '/../var/cache');
        $builder->writeProxiesToFile(true, __DIR__ . '/../var/cache/proxies');
    }

    $builder->addDefinitions($settings);
    $builder->addDefinitions($dependencies);

    $container = $builder->build();

    $app = \DI\Bridge\Slim\Bridge::create($container);

    $routes($app);

    $app->addRoutingMiddleware();

    $callableResolver = $app->getCallableResolver();
    $responseFactory = $app->getResponseFactory();

    $errorHandler = new AppErrorHandler($callableResolver, $responseFactory);
    $errorHandler->registerErrorRenderer('application/json', JsonErrorRenderer::class);
    $errorHandler->forceContentType('application/json');

    $errorMiddleware = $app->addErrorMiddleware($settings['app.display_error_details'], true, true, $container->get(LoggerInterface::class));
    $errorMiddleware->setDefaultErrorHandler($errorHandler);

    $routeCollector = $app->getRouteCollector();
    if ($settings['app.cache_enabled']) {
        $routeCollector->setCacheFile(__DIR__ . '/../var/cache/routes.cache.php');
    }

    return $app;
};
