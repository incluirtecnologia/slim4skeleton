<?php

use Doctrine\Migrations\Configuration\Migration\PhpFile;
use Doctrine\ORM\EntityManager;

require 'vendor/autoload.php';

$settings = require 'settings.php';
$config = new PhpFile(__DIR__ . '/../migrations.php');

$ormConfig = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
    $settings['doctrine.meta.entity_path'],
    (bool) $settings['doctrine.meta.auto_generate_proxies'],
    $settings['doctrine.meta.proxy_dir']
);

$platform = new \Doctrine\DBAL\Platforms\MySQL57Platform();
$settings['doctrine.connection']['platform'] = $platform;
$em = EntityManager::create($settings['doctrine.connection'], $ormConfig);


$helperSet = new \Symfony\Component\Console\Helper\HelperSet([
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
]);

return $helperSet;
