<?php

declare(strict_types=1);


namespace Intec\Slim4Skeleton\Test\Action;

use Intec\Slim4Skeleton\Test\TestCase;

class Test404FTest extends TestCase
{
    public function testInvalidRouteWillReturn404()
    {
        $resp = $this->runApp('GET', '/invalid-route');

        $this->assertEquals(404, $resp->getStatusCode());
    }
}