<?php

declare(strict_types=1);


namespace Intec\Slim4Skeleton\Test\Action;

use Intec\Slim4Skeleton\Test\TestCase;

class HealthCheckFTest extends TestCase
{
    public function testHealthzReturns200()
    {
        $resp = $this->runApp('GET', '/healthz');

        $this->assertEquals(200, $resp->getStatusCode());
    }
}