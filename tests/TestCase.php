<?php

declare(strict_types=1);

namespace Intec\Slim4Skeleton\Test;

use DI\Bridge\Slim\Bridge;
use DI\ContainerBuilder;
use Exception;
use PHPUnit\Framework\TestCase as PHPUnit_TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Headers;
use Slim\Psr7\Request as SlimRequest;
use Slim\Psr7\Uri;

class TestCase extends PHPUnit_TestCase
{
    protected $app;
    protected $container;
    protected $logger;

    public function setUp()
    {
        $this->app = $this->getAppInstance();
        $this->container = $this->app->getContainer();
        $this->logger =  $this->container->get(LoggerInterface::class);
    }

    /**
     * @return App
     * @throws Exception
     */
    protected function getAppInstance(): App
    {
        $appSettings = require __DIR__ . '/../config/settings.php';
        $testSettings = require __DIR__ . '/settings.php';
        $localTestSettings = file_exists('settings.local.php') ? require __DIR__ . '/settings.local.php' : [];

        $settings = array_merge($appSettings, $testSettings, $localTestSettings);
        $dependencies = require __DIR__ . '/../config/dependencies.php';
        $routes = require __DIR__ . '/../config/routes.php';
        $appBuilder = require __DIR__ . '/../config/app.php';

        $app = $appBuilder($settings, $dependencies, $routes);

        return $app;
    }

    /**
     * @param string $method
     * @param string $path
     * @param array  $headers
     * @param array  $serverParams
     * @param array  $cookies
     * @return Request
     */
    protected function createRequest(
        string $method,
        string $path,
        array $headers = ['HTTP_ACCEPT' => 'application/json'],
        array $serverParams = [],
        array $cookies = []
    ): Request {
        $uri = new Uri('', '', 80, $path);
        $handle = fopen('php://temp', 'w+');
        $stream = (new StreamFactory())->createStreamFromResource($handle);

        $h = new Headers();
        foreach ($headers as $name => $value) {
            $h->addHeader($name, $value);
        }

        return new SlimRequest($method, $uri, $h, $cookies, $serverParams, $stream);
    }

    protected function runApp(string $requestMethod, string $requestUri, array $requestData = [], array $headers = []): ResponseInterface
    {
        $request = $this->createRequest($requestMethod, $requestUri, $headers, $requestData);

        $this->logAppStartRun([$requestMethod, $requestUri, $requestData, $headers]);
        $resp = $this->app->handle($request);
        $this->logAppEndRun($resp);

        return $resp;
    }

    protected function runAppWithBearerAuthentication(
        string $bearerToken,
        string $requestMethod,
        string $requestUri,
        array $requestData = [],
        array $headers = []
    ): ResponseInterface {
        $headers = array_merge($headers, ['Authorization' => $this->createBearerHeader($bearerToken)]);

        return $this->runApp($requestMethod, $requestUri, $requestData, $headers);
    }

    protected function createBearerHeader(string $bearerToken)
    {
        return sprintf('Bearer %s', $bearerToken);
    }

    private function logAppStartRun(array $params)
    {
        $this->logger->info(json_encode($params, JSON_UNESCAPED_UNICODE ^ JSON_UNESCAPED_SLASHES));
    }

    protected function decodeResponse(ResponseInterface $response)
    {
        $contents = (string) $response->getBody();

        return json_decode($contents, true);
    }

    private function logAppEndRun(ResponseInterface $response)
    {
        if (json_last_error()) {
            $this->logger->info($response->getBody());

            return;
        }

        $data = $this->decodeResponse($response);
        $this->logger->info(json_encode($data, JSON_UNESCAPED_UNICODE ^ JSON_UNESCAPED_SLASHES));
    }

    public function tearDown()
    {
        $this->app = null;
        $this->container = null;
        $this->logger = null;
    }
}
