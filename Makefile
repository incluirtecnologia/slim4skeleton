init-hooks:
	git config core.hooksPath .githooks
semver:
	@sed -n 1p VERSION
up:
	docker-compose up
down:
	docker-compose down
db:
	docker exec -it slim4skeleton-sql bash -c "mysql -u mova -p'mova' mova"
php:
	docker exec -it slim4skeleton-php bash
logfile:
	touch logs/app.log
log:
	grc -c conf.log tail -f logs/app.log
install:
	composer install
update:
	composer update
migrate:
	???
migrate-status:
	???